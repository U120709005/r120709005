package myobserver;

public class HexaDecimalView extends Observer{
	
	
	public HexaDecimalView(NumberPublisher numberPublisher){
	      this.numberPublisher = numberPublisher;
	      this.numberPublisher.attach(this);
	   }

	@Override
	public void update() {
	      System.out.println( "HexaDEcimal View: " + Integer.toHexString( numberPublisher.getState() ).toUpperCase() ); 
	   }
}


