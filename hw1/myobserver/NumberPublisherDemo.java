package myobserver;

public class NumberPublisherDemo {
	
	public static void main(String[] args) throws InterruptedException {
	      NumberPublisher numberPublisher = new NumberPublisher();

	      new HexaDecimalView(numberPublisher);
	      new DecimalView(numberPublisher);
	      new BinaryView(numberPublisher);

	     // System.out.println("First state change: 15");	
	     // numberPublisher.setState(15);
	      //System.out.println("Second state change: 10");	
	      //numberPublisher.setState(10);
	      
	      int publishCount = 5;
		    
		    
		    for (int i =0; i<publishCount; i++){
				int number = i*20;
				System.out.println("\nPublishing:" + number);
				numberPublisher.setState(number);
				Thread.sleep(3000);
			}
		   }

}

