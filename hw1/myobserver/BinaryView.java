package myobserver;

public class BinaryView extends Observer {

	public BinaryView(NumberPublisher numberPublisher){
	      this.numberPublisher = numberPublisher;
	      this.numberPublisher.attach(this);
	   }

	   @Override
	   public void update() {
	      System.out.println( "Binary String: " + Integer.toBinaryString( numberPublisher.getState() ) ); 
	   }

}

