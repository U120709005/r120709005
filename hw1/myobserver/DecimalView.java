package myobserver;

public class DecimalView extends Observer {
	

	   public DecimalView(NumberPublisher numberPublisher){
	      this.numberPublisher = numberPublisher;
	      this.numberPublisher.attach(this);
	   }
	   @Override
	   public void update() {
	     System.out.println( "Octal String: " + ( numberPublisher.getState() ) ); 
	   }

}

