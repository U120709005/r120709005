package javaobserver;

public class NumberPublisherDemo {

	public static void main(String[] args) throws InterruptedException {
		
		NumberPublisher np = new NumberPublisher();
		

		java.util.Observer disp1 = new BinaryView();
		java.util.Observer disp2 = new DecimalView();
		java.util.Observer disp3 = new HexaDecimalView();
		
		
		np.addObserver(disp1);
		np.addObserver(disp2);
		np.addObserver(disp3);
		
		np.measurementsChanged();
		
		int publishCount=5;
		for (int i =0; i<publishCount; i++){
			int num = i*20;
			System.out.println("\nPublishing:" + num);
			np.measurementsChanged();
			Thread.sleep(1000);
		}

	}

}