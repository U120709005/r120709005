package javaobserver;

import java.util.Observable;

public class NumberPublisher extends Observable  {

	int num;
	
	
	public int getDecimalView(){
		
		return num;
	}
	public String getHexaDecimalView(){
		
		return(Integer.toHexString(num).toUpperCase());
	}
	public String getBinaryView(){
	
		return(Integer.toBinaryString(num));
	}
	
	public void measurementsChanged() {
		setChanged();
		notifyObservers();
	}
}
