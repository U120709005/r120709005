package javaobserver;

import java.util.Observable;

public  class DecimalView implements java.util.Observer {

	
	public void update(Observable o, Object arg) {
		
		NumberPublisher np = (NumberPublisher) o;
		System.out.println("DecimalView :" + np.getDecimalView() );
		
	}
	
	

	
}