package test;

import component.CarType;
import component.concreteComponents.Sity;

import decorators.concreteDecorator.Airbag;
import decorators.concreteDecorator.AutamaticBreakingSystem;
import decorators.concreteDecorator.MusicSystem;

public class testCar {

	public static void main(String[] args) {
		CarType car = new Sity();

		car = new Airbag(car);

		car = new AutamaticBreakingSystem(car);
		
		car=new MusicSystem(car);

		System.out.println(car.getDescription() + " - DKK " + car.cost());
	}

}
