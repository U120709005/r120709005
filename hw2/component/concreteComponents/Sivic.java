package component.concreteComponents;
import component.CarType;

public class Sivic extends CarType {
	public Sivic() {
		description = "Sivic";
	}

	@Override
	public int cost() {
		return 400000;
	}

}
