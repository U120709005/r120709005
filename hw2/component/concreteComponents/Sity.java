package component.concreteComponents;
import component.CarType;

public class Sity extends CarType{
	public Sity() {
		description = "Sity";
	}

	@Override
	public int cost() {
		return 500000;
	}

}
