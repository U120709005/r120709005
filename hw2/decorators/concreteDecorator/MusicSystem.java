package decorators.concreteDecorator;

import component.CarType;

import decorators.ExtraEquipment;

public class MusicSystem extends ExtraEquipment {

	public  MusicSystem(CarType car) {
		this.car = car;
	}

	@Override
	public String getDescription() {
		return car.getDescription() + ", MusicSystem";
	}

	@Override
	public int cost() {
		return 1000 + car.cost();
	}

}


