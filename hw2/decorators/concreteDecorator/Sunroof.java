package decorators.concreteDecorator;


import component.CarType;

import decorators.ExtraEquipment;

public class Sunroof extends ExtraEquipment {

	public  Sunroof(CarType car) {
		this.car = car;
	}

	@Override
	public String getDescription() {
		return car.getDescription() + ", Sunroof";
	}

	@Override
	public int cost() {
		return 2000 + car.cost();
	}

}


