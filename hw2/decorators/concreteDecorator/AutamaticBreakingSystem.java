package decorators.concreteDecorator;


import component.CarType;

import decorators.ExtraEquipment;

public class AutamaticBreakingSystem extends ExtraEquipment {

	public AutamaticBreakingSystem(CarType car) {
		this.car = car;
	}

	@Override
	public String getDescription() {
		return car.getDescription() + ", Autamatic Breaking System";
	}

	@Override
	public int cost() {
		return 5000 + car.cost();
	}

}

