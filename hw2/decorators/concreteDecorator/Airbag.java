package decorators.concreteDecorator;

import component.CarType;

import decorators.ExtraEquipment;

public class Airbag extends ExtraEquipment {

	public Airbag(CarType car) {
		this.car = car;
	}

	@Override
	public String getDescription() {
		return car.getDescription() + ", Airbag";
	}

	@Override
	public int cost() {
		return 3000 + car.cost();
	}

}

